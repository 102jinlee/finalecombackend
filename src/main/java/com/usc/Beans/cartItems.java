package com.usc.Beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "usc_ecom_cart_items")
public class cartItems {
	private static final long serialVersionUID = 1L;
	@Id // primary key
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "usershopcart_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private UserShopcart usershopcart;
	
	@JoinColumn(name = "product_id")
	@ManyToOne(fetch = FetchType.EAGER)
	private Product product;
	
	@Column(name = "qty", nullable = false) // @column define a column name
	private int qty;

	public cartItems() {
		super();
		// TODO Auto-generated constructor stub
	}

	public cartItems(int id, int qty) {
		super();
		this.id = id;
		this.qty = qty;
	}
	
	

	public cartItems(Product product, int qty) {
		super();
		this.product = product;
		this.qty = qty;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public UserShopcart getUsershopcart() {
		return usershopcart;
	}

	public void setUsershopcart(UserShopcart usershopcart) {
		this.usershopcart = usershopcart;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
