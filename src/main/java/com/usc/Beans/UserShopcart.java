package com.usc.Beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "usc_ecom_user_shopcart")
public class UserShopcart {
	
	private static final long serialVersionUID = 1L;
	@Id // primary key
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@JsonIgnore
	@JoinColumn(name = "user_id")
	@OneToOne(fetch = FetchType.EAGER)
	private User user;
	
	@OneToMany(mappedBy = "usershopcart", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<cartItems> cartitems;

	public UserShopcart() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserShopcart(int id, User user) {
		super();
		this.id = id;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
	public List<cartItems> getCartitems() {
		return cartitems;
	}

	public void setCartitems(List<cartItems> cartitems) {
		this.cartitems = cartitems;
	}

	@Override
	public String toString() {
		return "UserShopcart [id=" + id + ", user=" + user + ", cartitems=" + cartitems + "]";
	}

	
}
