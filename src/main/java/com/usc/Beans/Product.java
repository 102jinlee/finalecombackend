package com.usc.Beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usc_ecom_product")
public class Product {
	private static final long serialVersionUID = 1L;
	@Id // primary key
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "product_name", nullable = false) // @column define a column name
	private String productname;

	@Column(name = "category", nullable = false) // @column define a column name
	private String category;
	
	@Column(name = "price", nullable = false) // @column define a column name
	private float price;
	
	@Column(name = "product_desc") // @column define a column name
	private String productDesc;
	
	@Column(name = "qty_stock", nullable = false) // @column define a column name
	private int qty_stock;
	
	@Column(name = "discount", nullable = false) // @column define a column name
	private float discount;
	
	@Column(name = "imgUrl", nullable = false) // @column define a column name
	private String imgUrl;

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Product(int id, String productname, String category, float price, String productDesc, int qty_stock,
			float discount, String imgUrl) {
		super();
		this.id = id;
		this.productname = productname;
		this.category = category;
		this.price = price;
		this.productDesc = productDesc;
		this.qty_stock = qty_stock;
		this.discount = discount;
		this.imgUrl = imgUrl;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public int getQty_stock() {
		return qty_stock;
	}

	public void setQty_stock(int qty_stock) {
		this.qty_stock = qty_stock;
	}

	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", productname=" + productname + ", category=" + category + ", price=" + price
				+ ", productDesc=" + productDesc + ", qty_stock=" + qty_stock + ", discount=" + discount + ", imgUrl="
				+ imgUrl + "]";
	}
	
	
	
	
}
