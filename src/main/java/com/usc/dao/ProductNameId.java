package com.usc.dao;

public interface ProductNameId {
	int getId();
	String getProductname();
}
