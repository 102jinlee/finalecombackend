package com.usc.dao;

public interface UsernameOnly {
	String getUsername();
}
