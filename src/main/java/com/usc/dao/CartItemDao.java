package com.usc.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usc.Beans.cartItems;

public interface CartItemDao extends JpaRepository<cartItems, Integer> {
	List<cartItems> findByUsershopcartId(int userShopcartId);
	
}
