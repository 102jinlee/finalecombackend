package com.usc.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.usc.Beans.Product;

@Repository
public interface ProductDao extends JpaRepository<Product, Integer> {
	Product findByProductname(String productname);
	List<Product> findByCategory(String category);
	List<ProductNameId> findDistinctAllBy();
}
