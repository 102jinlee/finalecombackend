package com.usc.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usc.Beans.OrderItems;


public interface OrderItemDao extends JpaRepository<OrderItems, Integer> {
	List<OrderItems> findByOrderId(int id);
}
