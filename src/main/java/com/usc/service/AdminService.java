package com.usc.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usc.Beans.Product;
import com.usc.dao.OrderDao;
import com.usc.dao.OrderItemDao;
import com.usc.dao.ProductDao;
import com.usc.dao.UserDao;
import com.usc.http.Response;

@Service
@Transactional
public class AdminService {
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	ProductDao productDao;
	
	@Autowired
	OrderItemDao orderItemDao;
	
	
	public Response addProduct(Product product) {
		System.out.println(product.toString());
		productDao.save(product);
		return new Response(true);
	}
	
	public Response deleteProduct(int id) {
		productDao.deleteById(id);
		return new Response(true);
	}
	
	public Response changeProduct(Product product) {
		Product p = productDao.findById(product.getId()).get();
		p.setProductname(product.getProductname());
		p.setProductDesc(product.getProductDesc());
		p.setCategory(product.getCategory());
		p.setDiscount(product.getDiscount());
		p.setPrice(product.getPrice());
		p.setQty_stock(product.getQty_stock());
		p.setImgUrl(product.getImgUrl());
		productDao.save(p);
		return new Response(true);
	}
	
}
