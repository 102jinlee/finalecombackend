package com.usc.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.usc.Beans.Order;
import com.usc.Beans.OrderItems;
import com.usc.Beans.Product;
import com.usc.Beans.User;
import com.usc.dao.OrderDao;
import com.usc.dao.OrderItemDao;
import com.usc.dao.ProductDao;
import com.usc.dao.UserDao;
import com.usc.http.Response;

@Service
@Transactional
public class OrderService {

	@Autowired
	OrderDao orderDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	ProductDao productDao;
	
	@Autowired
	OrderItemDao orderItemDao;
	
	public Response addOrder(Order order, Authentication authentication) {
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
		    String currentUserName = authentication.getName();
		    User u = userDao.findByUsername(currentUserName);
		    order.setUser(u);
		    List<OrderItems> orderItems = order.getOrderItems();
		    for (OrderItems item: orderItems) {
		    	Product product = (Product) productDao.findById(item.getProduct().getId()).get();
		    	item.setProduct(product);
		    	item.setOrder(order);
		    }
		    orderDao.save(order);
		   
		}
		else {
			return new Response(false);
		}
		return new Response(true);
	}
	
//	public Response addOrder(Order order, Authentication authentication) {
//		if (!(authentication instanceof AnonymousAuthenticationToken)) {
//		    String currentUserName = authentication.getName();
//		    User u = userDao.findByUsername(currentUserName);
//		    order.setUser(u);
//		    Order curOrder = orderDao.save(order);
////		    List<OrderItems> orderItems = new ArrayList<OrderItems>();
////		    Order curOrder = orderDao.findByUserId(u.getId());
//		    System.out.println(curOrder.toString());
//		    List<OrderItems> orderItems = curOrder.getOrderItems();
//		    curOrder.setOrderItems(new ArrayList<OrderItems>());
//		    for (OrderItems item: orderItems) {
//		    	System.out.println(item.toString());
//		    	curOrder.getOrderItems().add(item);
//		    	item.setOrder(curOrder);
//		    	System.out.println(curOrder.toString());
//		    	System.out.println(curOrder.getId());
////		    	 orderItemDao.save(item);
//		    	System.out.println(item.toString());
////		    	orderItems.add(item);
//		    }
////		    orderItemDao.saveAll(orderItems);
//		   
//		}
//		else {
//			return new Response(false);
//		}
//		return new Response(true);
//	}
}
