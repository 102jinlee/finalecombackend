package com.usc.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.usc.Beans.Product;
import com.usc.Beans.User;
import com.usc.Beans.UserShopcart;
import com.usc.Beans.caritemWrapper;
import com.usc.Beans.cartItems;
import com.usc.dao.CartItemDao;
import com.usc.dao.ProductDao;
import com.usc.dao.UserDao;
import com.usc.http.Response;

@Service
@Transactional
public class ShopcartService {

	@Autowired
	UserDao userDao;
	
	@Autowired
	ProductDao productDao;
	
	@Autowired
	CartItemDao cartitemDao;
	
	
	public Response addProduct(Product product, int qty, Authentication authentication) {
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			cartItems cartitem = new cartItems(product, qty);
		    String currentUserName = authentication.getName();
		    User u = userDao.findByUsername(currentUserName);
		    UserShopcart ushopcart = u.getUserShopcart();
		    ushopcart.setUser(u);
		    cartitem.setUsershopcart(ushopcart);
		    cartitemDao.save(cartitem);
		   
		}
		else {
			return new Response(false);
		}
		return new Response(true);
	}
	
	public Response deleteCartItems(caritemWrapper caritemWrap, Authentication authentication) {
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String currentUserName = authentication.getName();
		    User u = userDao.findByUsername(currentUserName);
		    UserShopcart ushopcart = u.getUserShopcart();
		    List<cartItems> cartitems = caritemWrap.getCaritems();
		    cartitems.forEach(cartItem -> {
		    	cartItem.setUsershopcart(ushopcart);
		    });
		    cartitemDao.deleteInBatch(cartitems);
		}
		else {
			return new Response(false);
		}
		return new Response(true);
	}
	
}
