package com.usc.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.Beans.Order;
import com.usc.Beans.Product;
import com.usc.Beans.User;
import com.usc.dao.OrderDao;
import com.usc.dao.OrderItemDao;
import com.usc.dao.UserDao;
import com.usc.http.Response;
import com.usc.service.OrderService;

@RestController() // recept API request, auto return json
@RequestMapping("/order")
public class OrderController {
	
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	OrderItemDao orderItemDao;
	
	@Autowired
	OrderService orderService;
	
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@GetMapping
	public List<Order> getorders(Authentication authentication){
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
		    String currentUserName = authentication.getName();
		    User currentUser = userDao.findByUsername(currentUserName);
		    return orderDao.findByUserId(currentUser.getId());
		}
		else
			return Collections.emptyList();
	}
	
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_USER')")
	@PostMapping //(headers = "Accept=application/json")
	public Response addOrder(@RequestBody Order order, Authentication authentication) { // json
		return orderService.addOrder(order, authentication);
	}
	
	
}
