package com.usc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.Beans.Order;
import com.usc.Beans.Product;
import com.usc.Beans.User;
import com.usc.dao.OrderDao;
import com.usc.dao.OrderItemDao;
import com.usc.dao.ProductDao;
import com.usc.dao.ProductNameId;
import com.usc.dao.UserDao;
import com.usc.dao.UsernameOnly;
import com.usc.http.Response;
import com.usc.service.AdminService;

@RestController() // recept API request, auto return json
@RequestMapping("/admin")
public class AdminManageController {
	@Autowired
	ProductDao productDao;
	
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	OrderItemDao orderItemDao;
	
	@Autowired
	AdminService adminService;
	
	//@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@PostMapping
	public Response addProduct(@RequestBody Product product) {
		return adminService.addProduct(product);
	}
	
	//@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@DeleteMapping("/{id}") 
	public Response deleteProduct(@PathVariable int id) {
		return adminService.deleteProduct(id);
	}
	
	//@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@PutMapping
	public Response changeProduct(@RequestBody Product product) {
		return adminService.changeProduct(product);
	}
	
	//@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@GetMapping("/orders")
	public List<Order> getAllOrder(){
		return orderDao.findAll();
	}
	
	//@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@GetMapping("/orders/{username}")
	public List<Order> getOrderByUsername(@PathVariable String username){
		User u = userDao.findByUsername(username);
		List<Order> orders = orderDao.findByUserId(u.getId());
		return orders;
	}
	
	//@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@GetMapping("/usernames")//findDistinctAllBy
	public List<UsernameOnly> getAllUsername(){
		return userDao.findDistinctAllBy();
	}
}
