package com.usc.controller;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.Beans.Product;
import com.usc.Beans.User;
import com.usc.Beans.UserShopcart;
import com.usc.Beans.caritemWrapper;
import com.usc.Beans.cartItems;
import com.usc.dao.CartItemDao;
import com.usc.dao.ProductDao;
import com.usc.dao.ProductNameId;
import com.usc.dao.UserDao;
import com.usc.http.Response;
import com.usc.service.ShopcartService;

@RestController() // recept API request, auto return json
@RequestMapping("/products")
public class ProductController {
	@Autowired
	ProductDao productDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	CartItemDao cartitemDao;
	
	@Autowired
	ShopcartService shopcartService;
	
	
	@GetMapping
	public List<Product> getAllProduct(){
		return productDao.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Product> getProductById(@PathVariable int id){
		return productDao.findById(id);
	}
	
	//@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	@GetMapping("/IdName")
	public List<ProductNameId> getAllProductNameId(){
		return productDao.findDistinctAllBy();
	}
	
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_USER')")
	@PostMapping("/addtocart/{qty}")
	public Response addProductToCart(@RequestBody Product product, @PathVariable int qty, Authentication authentication) {
		System.out.println("========");
		return shopcartService.addProduct(product, qty, authentication);
	}
	
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_USER')")
	@GetMapping("/shopcart")
	public List<cartItems> getShopcartItems(Authentication authentication){
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String currentUserName = authentication.getName();
		    User u = userDao.findByUsername(currentUserName);
		    UserShopcart ushopcart = u.getUserShopcart();
			return cartitemDao.findByUsershopcartId(ushopcart.getId());
		}
		return Collections.emptyList();
	}
	
	@DeleteMapping("/deleteCart")
	public Response deleteCart(@RequestBody caritemWrapper carItemwrap, Authentication authentication) {
		return shopcartService.deleteCartItems(carItemwrap, authentication);
	}
}
